ruby-serverengine (2.3.0-1) experimental; urgency=medium

  * New upstream release

  [ Hideki Yamane ]
  * debian/patches
    - Refresh patches
    - Add 0006-Explicity-specifying-require-fileutils.patch
    - Add 0007-Drop-unnecessary-rspec-settings.patch
    - Add 0008-Drop-unnecessary-bundler.patch
  * debian/control
    - Set Standards-Version: 4.6.1
    - Enable autopkgtest-pkg-ruby
    - Drop unnecessary dependency: bundler
    - Add ruby-timecop for tests
  * Upload to experimental due to several patches to adjust dependency
    versions with upstream gemspec, and also failed autopkgtest on salsa-ci.
    Anyway push out once it as package.

  [ Kentaro Hayashi ]
  * Fix incomplete debian/watch rule

 -- Hideki Yamane <henrich@debian.org>  Mon, 03 Oct 2022 05:46:27 +0900

ruby-serverengine (2.2.2-1) unstable; urgency=medium

  * New upstream release

 -- Hideki Yamane <henrich@debian.org>  Sun, 15 Nov 2020 14:59:08 +0900

ruby-serverengine (2.2.1-1) unstable; urgency=medium

  [ Hideki Yamane ]
  * New upstream release
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * debian/control
    - Set Build-Depends: debhelper-compat (= 13)
    - Once disable autopkgtest-pkg-ruby
  * Rename debian/ruby-tests.rake to be disabled as well

  [ Daniel Leidert ]
  * d/watch: Name downloaded file.

 -- Hideki Yamane <henrich@debian.org>  Tue, 23 Jun 2020 21:21:37 +0900

ruby-serverengine (2.2.0-1) unstable; urgency=medium

  * New upstream release
    - Fix dual stack mode issue for tcp socket with IPv6

  * debian/gbp.conf
    - adjust gbp.conf to avoid CI failure
  * debian/control
    - add dependency versions from gemspec and note about rake-compiler-dock
    - set Standards-Version: 4.4.1
    - drop Build-Depends: debhelper
  * debian/patches
    - add 0005-use-require_relative-instead.patch
  * debian/salsa-ci.yml
    - update to not check arch-depend things

 -- Hideki Yamane <henrich@debian.org>  Wed, 20 Nov 2019 15:04:07 +0900

ruby-serverengine (2.1.1-2) unstable; urgency=medium

  * debian/control
    - set Standards-Version: 4.4.0
    - add dependency versions from gemspec
    - note about rake-compiler-dock
  * debian/salsa-ci.yml
    - add salsa-ci pipeline setting
  * debian/gbp.conf
    - adjust settings to avoid CI failure

 -- Hideki Yamane <henrich@debian.org>  Mon, 12 Aug 2019 18:02:39 +0900

ruby-serverengine (2.1.1-1) unstable; urgency=medium

  * Upstream release 2.1.1

 -- Hideki Yamane <henrich@debian.org>  Fri, 17 May 2019 09:53:53 +0900

ruby-serverengine (2.1.0-1) unstable; urgency=medium

  * Initial release (Closes: #926572)

 -- Hideki Yamane <henrich@debian.org>  Wed, 03 Apr 2019 04:32:59 +0900
